Instructions

In mapper directory, type "venv/bin/python -m mapper" RETURN

Click on url after "Running on "

In program

Find gpx file to upload.
Submit query

Change Tile server to "https://tile.tracestrack.com/topo_en/{z}/{x}/{y}.png?" with API key pasted on the end

(Check out https://wiki.openstreetmap.org/wiki/Raster_tile_providers for alternatives)
Change detail level, margin, colour and width if required.

Right click on map to save image file.

In terminal, CTRL-C to stop program.

To reduce file size.

Go to directory with image file in.

"zopflipng filename.png newfilename.png" RETURN




from __future__ import annotations

from math import log, pi, tan, tau, atan, sin, cos
from trig import sec, hvs, ahvs

a = 6_378_137
f = 1 / 298.257223563


def geodetic_to_tile(*, λ: float, φ: float) -> tuple[float, float]:
    return ((λ / tau + 0.5), (1 - (log(tan(φ) + sec(φ)) / pi)) / 2.0)


def distance(*, λ0: float, φ0: float, λ1: float, φ1: float) -> float:
    β0 = reduced_latitude(φ0)
    β1 = reduced_latitude(φ1)
    P = (β0 + β1) / 2
    Q = (β1 - β0) / 2
    σ = central_angle(λ0=λ0, φ0=β0, λ1=λ1, φ1=β1)
    if σ == 0:
        return 0
    X = (σ - sin(σ)) * sin(P) ** 2 * cos(Q) ** 2 / cos(σ / 2) ** 2
    Y = (σ + sin(σ)) * cos(P) ** 2 * sin(Q) ** 2 / sin(σ / 2) ** 2
    return a * (σ - f / 2 * (X + Y))


def reduced_latitude(φ: float) -> float:
    return atan((1 - f) * tan(φ))


def central_angle(*, λ0: float, φ0: float, λ1: float, φ1: float):
    Δφ = φ1 - φ0
    Δλ = λ1 - λ0
    return ahvs(hvs(Δφ) + (1 - hvs(Δφ) - hvs(φ0 + φ1)) * hvs(Δλ))

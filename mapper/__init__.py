from __future__ import annotations

import base64
import hashlib
import itertools
import json
import math
from dataclasses import dataclass
from io import BytesIO
from math import sqrt
from pathlib import Path
from typing import TYPE_CHECKING

import lxml.etree
import platformdirs
import requests
from cvectors import Vector
from flask import Flask, abort, current_app, request, send_file
from PIL import Image, ImageDraw, ImageFont

from . import coords

if TYPE_CHECKING:
    from collections.abc import Iterable
    from typing import IO, Self

    from flask import Response

CACHE_DIR = Path(platformdirs.user_cache_dir("mapper"))
GPX_DIR = CACHE_DIR / "gpxs"
TILE_DIR = CACHE_DIR / "tiles"

app = Flask(__name__)


def get_tile(server: str, zoom: int, x: int, y: int) -> Image.Image:
    encoded_server = base64.b64encode(server.encode("utf-8"), altchars=b"_-").decode(
        "utf-8"
    )
    path = TILE_DIR / f"{encoded_server}" / str(zoom) / str(x) / f"{y}.png"
    if not path.exists():
        response = requests.get(
            server.format(z=zoom, x=x, y=y),
            headers={"User-Agent": "GPX Map Renderer (dev.)"},
        )
        path.parent.mkdir(parents=True, exist_ok=True)
        with path.open("wb") as f:
            for chunk in response.iter_content(chunk_size=128):
                f.write(chunk)
    return Image.open(path)


def render_grid(server: str, view: View) -> tuple[Image.Image, tuple[int, int]]:
    x_range = range(int(2**view.zoom * view.x_min), int(2**view.zoom * view.x_max + 1))
    y_range = range(int(2**view.zoom * view.y_min), int(2**view.zoom * view.y_max + 1))
    if len(x_range) * len(y_range) > 200:
        abort(400)

    first_pos = (x_range.start, y_range.start)
    first_tile = get_tile(server, view.zoom, *first_pos)
    tile_size = (first_tile.width, first_tile.height)
    image = Image.new("RGB", (len(x_range) * tile_size[0], len(y_range) * tile_size[1]))
    for x in x_range:
        for y in y_range:
            tile = (
                first_tile if (x, y) == first_pos else get_tile(server, view.zoom, x, y)
            )
            image.paste(
                tile,
                ((x - first_pos[0]) * tile_size[0], (y - first_pos[1]) * tile_size[1]),
            )
    image = image.crop(
        (
            ((2**view.zoom * view.x_min) - x_range.start) * tile_size[0],
            ((2**view.zoom * view.y_min) - y_range.start) * tile_size[1],
            ((2**view.zoom * view.x_max) - x_range.start) * tile_size[0],
            ((2**view.zoom * view.y_max) - y_range.start) * tile_size[1],
        )
    )
    return (image, tile_size)


def parse_gpx(gpx: IO[bytes]) -> list[list[tuple[float, float, float]]]:
    tree = lxml.etree.parse(gpx)
    d = 0.0
    result = []
    last = None
    for seg in tree.findall("./{*}trk/{*}trkseg"):
        track = []
        for pt in seg.findall("{*}trkpt"):
            φ = math.radians(float(pt.attrib["lat"]))
            λ = math.radians(float(pt.attrib["lon"]))
            if last is not None:
                d += coords.distance(φ0=last["φ"], λ0=last["λ"], φ1=φ, λ1=λ)
            last = {"φ": φ, "λ": λ}
            track.append((*coords.geodetic_to_tile(φ=φ, λ=λ), d))
        result.append(track)
    return result


def save_gpx(gpx: IO[bytes]) -> str:
    data = parse_gpx(gpx)
    json_blob = json.dumps(data, separators=(",", ":")).encode("utf-8")
    h = hashlib.sha256(json_blob).hexdigest()[:32]
    GPX_DIR.mkdir(parents=True, exist_ok=True)
    (GPX_DIR / f"{h}.json").write_bytes(json_blob)
    return h


def load_gpx(h: str) -> list[list[list[float]]]:
    with (GPX_DIR / f"{h}.json").open() as f:
        return json.load(f)


@app.route("/upload-gpx", methods=["POST"])
def upload():
    if "file" not in request.files:
        abort(400, "no file provided")
    f = request.files["file"]
    if not (f.filename and f):
        abort(400, "invalid file")
    gpx_id = save_gpx(f)

    gpx = load_gpx(gpx_id)
    length = gpx[-1][-1][2]
    distance_marker_interval = next(
        i for i in possible_distance_marker_intervals() if length / (1000 * i) <= 20
    )
    view = View.default_of(gpx)
    return {
        "gpx_id": gpx_id,
        "view": view.to_client_json(),
        "distance_marker_interval": distance_marker_interval,
    }


def draw_track(
    gpx: list[list[list[float]]],
    size: tuple[int, int],
    view: View,
    line_width: float,
    colour: str,
    tile_size: tuple[int, int],
    show_start_end: bool,
    distance_marker_interval: int,
    unit: float,
) -> Image.Image:
    AA = 4
    scaled_image = Image.new("RGBA", (size[0] * AA, size[1] * AA), (0, 0, 0, 0))
    draw = ImageDraw.Draw(scaled_image)
    top_left = (2**view.zoom * view.x_min, 2**view.zoom * view.y_min)
    overall_scale = sqrt(size[0] * size[1] / 256**2)

    def to_pixel(x, y):
        return (
            AA * tile_size[0] * (2**view.zoom * x - top_left[0]),
            AA * tile_size[1] * (2**view.zoom * y - top_left[1]),
        )

    marker_size = 2.2 * AA * line_width * overall_scale
    for seg in gpx:
        draw.line(
            [to_pixel(x, y) for [x, y, _] in seg],
            joint="curve",
            fill=colour,
            width=round(AA * line_width * overall_scale),
        )
        if show_start_end:
            for (x, y, _), circle_colour in ((seg[0], "#00aa00"), (seg[-1], "#ff0000")):
                draw.circle(to_pixel(x, y), round(marker_size), fill="#ffffff")
                draw.circle(
                    to_pixel(x, y), round(0.7 * marker_size), fill=circle_colour
                )

    k = distance_marker_interval * unit
    font = ImageFont.load_default(marker_size)
    for seg in gpx:
        for [x0, y0, d0], [x1, y1, d1] in itertools.pairwise(seg):
            n = int(d1 / k)
            if n > int(d0 / k):
                t = (n * k - d0) / (d1 - d0)
                p = to_pixel(x0 + (x1 - x0) * t, y0 + (y1 - y0) * t)
                draw.circle(p, round(marker_size), fill="#ffffff")

                draw.text(
                    p,
                    str(n * distance_marker_interval),
                    fill=(0, 0, 0),
                    anchor="mm",
                    font=font,
                )

    return scaled_image.resize(size, resample=Image.BOX)


@app.route("/render")
def render() -> Response:
    server = request.args.get("server", type=str)
    line_width = request.args.get("width", type=float)
    colour = request.args.get("colour", type=str)
    gpx = load_gpx(request.args.get("gpx"))
    credit = request.args.get("credit", type=str)
    show_start_end = request.args.get("show_start_end", type=bool)
    distance_marker_interval = request.args.get("distance_marker", type=int)
    distance_unit = {"km": 1000, "mi": 1609.344}[
        request.args.get("distance_unit", type=str)
    ]
    view = View.from_request_arguments(TrackBoundingBox.of(gpx), request.args)
    image, tile_size = render_grid(server, view)
    track = draw_track(
        gpx,
        image.size,
        view,
        line_width,
        colour,
        tile_size,
        show_start_end,
        distance_marker_interval,
        distance_unit,
    )
    image.paste(track, (0, 0), track)
    ImageDraw.Draw(image).text(
        image.size,
        credit,
        fill=(0, 0, 0),
        anchor="rb",
        font=ImageFont.load_default(3 * sqrt(image.size[0] * image.size[1] / 256**2)),
    )
    result = BytesIO()
    image.save(result, format="PNG")
    result.seek(0)
    return send_file(result, mimetype="image/png")


def possible_distance_marker_intervals() -> int:
    base = 1
    while True:
        yield base
        yield 2 * base
        yield 5 * base
        base *= 10


@dataclass
class TrackBoundingBox:
    centre: Vector
    size: Vector

    @classmethod
    def of(cls, gpx: Iterable[Iterable[list[float]]]) -> Self:
        mins = Vector(
            min(x for s in gpx for (x, y, _) in s),
            min(y for s in gpx for (x, y, _) in s),
        )
        maxs = Vector(
            max(x for s in gpx for (x, y, _) in s),
            max(y for s in gpx for (x, y, _) in s),
        )
        return cls((maxs + mins) / 2, (maxs - mins))


@dataclass
class View:
    zoom: int
    aspect_ratio: float
    margin: float
    track_bounding_box: TrackBoundingBox

    @classmethod
    def default_of(cls, gpx: Iterable[Iterable[list[float]]]) -> Self:
        track_bounding_box = TrackBoundingBox.of(gpx)
        margin = 0.05
        x_range = track_bounding_box.size.x * (1 + margin * 2)
        y_range = track_bounding_box.size.y * (1 + margin * 2)
        aspect_ratio = sqrt(2)
        if x_range < y_range:
            aspect_ratio = 1 / aspect_ratio

        if x_range > y_range * aspect_ratio:
            y_range = x_range / aspect_ratio
        else:
            x_range = y_range * aspect_ratio

        zoom = int(0.5 * math.log2(20 / (x_range * y_range)))
        return cls(zoom, aspect_ratio, margin, track_bounding_box)

    def to_client_json(self) -> dict[str, float]:
        return {
            "zoom": self.zoom,
            "aspect_ratio": self.aspect_ratio,
            "margin": self.margin,
        }

    @classmethod
    def from_request_arguments(cls, track_bounding_box: TrackBoundingBox, args) -> Self:
        return cls(
            args.get("zoom", type=int),
            args.get("aspect_ratio", type=float),
            args.get("margin", type=float),
            track_bounding_box,
        )

    @property
    def x_range(self) -> float:
        ranges = self.track_bounding_box.size * (1 + self.margin * 2)
        return max(ranges.x, ranges.y * self.aspect_ratio)

    @property
    def y_range(self) -> float:
        ranges = self.track_bounding_box.size * (1 + self.margin * 2)
        return max(ranges.y, ranges.x / self.aspect_ratio)

    @property
    def x_min(self) -> float:
        return self.track_bounding_box.centre.x - self.x_range / 2

    @property
    def x_max(self) -> float:
        return self.track_bounding_box.centre.x + self.x_range / 2

    @property
    def y_min(self) -> float:
        return self.track_bounding_box.centre.y - self.y_range / 2

    @property
    def y_max(self) -> float:
        return self.track_bounding_box.centre.y + self.y_range / 2


@app.route("/")
def home() -> str:
    return current_app.send_static_file("index.html")


def main() -> None:
    app.run(debug=True)
    return 0

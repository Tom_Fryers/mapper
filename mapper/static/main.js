{
    const input = document.getElementById("margin");
    const output = document.getElementById("margin-output");

    input.addEventListener("input", () => {
        output.textContent = input.value;
    });
}
{
    const select = document.getElementById("aspect-ratio");
    function setCustomVisibility() {
        document.getElementById("custom-aspect-ratio").hidden =
            select.selectedIndex !== 0;
    }
    setCustomVisibility();
    select.addEventListener("change", setCustomVisibility);

    const input = document.getElementById("custom-aspect-ratio");
    const output = document.getElementById("custom-aspect-ratio-option");

    output.value = input.value;
    input.addEventListener("input", () => {
        output.value = input.value;
    });
}
function setupStorage(inputId) {
    const input = document.getElementById(inputId);
    const storedInput = localStorage.getItem(inputId);
    if (storedInput) {
        input.value = storedInput;
    }
    input.addEventListener("change", () => {
        localStorage.setItem(inputId, input.value);
    });
}
setupStorage("server");
setupStorage("credit");

function get(url, parameters) {
    const components = Object.entries(parameters)
        .map(([k, v]) => `${encodeURIComponent(k)}=${encodeURIComponent(v)}`)
        .join("&");
    if (components) {
        return `${url}?${components}`;
    }
    return url;
}

async function upload() {
    const form = document.getElementById("upload-form");
    const formData = new FormData(form);
    const response = await fetch("/upload-gpx", {
        method: "POST",
        body: formData,
        enctype: "multipart/form-data",
    });
    const data = JSON.parse(await response.text());
    gpx_id = data.gpx_id;

    document.getElementById("zoom").value = data.view.zoom;

    const aspect_ratio = data.view.aspect_ratio;
    document.getElementById("aspect-ratio").value =
        [1, 1.4142135623730951, 0.7071067811865475].includes(aspect_ratio) ?
            aspect_ratio
        :   "Custom";

    document.getElementById("margin").value = data.view.margin;
    document.getElementById("margin-output").value = data.view.margin;

    document.getElementById("distance-marker").value =
        data.distance_marker_interval;

    render();
}

function render() {
    const params = {
        gpx: gpx_id,
        server:
            document.getElementById("server").value
            || document.getElementById("server").placeholder,
        credit:
            document.getElementById("credit").value
            || document.getElementById("credit").placeholder,
        zoom: document.getElementById("zoom").value,
        aspect_ratio: document.getElementById("aspect-ratio").value,
        margin: document.getElementById("margin").value,
        colour: document.getElementById("colour").value,
        width: document.getElementById("width").value,
        distance_marker: document.getElementById("distance-marker").value,
        distance_unit: document.getElementById("distance-unit").value,
    };
    if (document.getElementById("show-start-end").checked) {
        params.show_start_end = true;
    }
    document.getElementById("map").src = get("/render", params);
}
window.onload = function () {
    document
        .getElementById("param-form")
        .addEventListener("submit", function (e) {
            e.preventDefault();
            render();
        });
    document
        .getElementById("upload-form")
        .addEventListener("submit", function (e) {
            e.preventDefault();
            upload();
        });
};
